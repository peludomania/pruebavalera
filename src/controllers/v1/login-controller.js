const index = async (req, res) => {
  try {
    
    let loginSuccess;
    switch (req.body.email) {
      case process.env.USER1:
        loginSuccess = (process.env.PASSWORD1 !== req.body.password)
        break;
      case process.env.USER2:
        loginSuccess = (process.env.PASSWORD2 !== req.body.password)
        break;
      default:
        loginSuccess = false;
        break;
    }
    
    if (loginSuccess) {
      res.status(403).send({ status: 'ERROR', message: 'Login failed' });
    } else {
      res.send({
        success: true
      });
    }

  } catch (e) {
    res.status(500).send({ status: 'ERROR', message: e.message });
  }
};

module.exports = {
  index,
};
