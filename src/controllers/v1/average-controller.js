const index = async (req, res) => {
  try {
    let sum = 0;

    for (const number of req.query.n) {
      sum += parseInt( number, 10 );
    }

    let average = sum/req.query.n.length;
    
    res.send({
      average
    });
      
  } catch (e) {
    res.status(500).send({ status: 'ERROR', message: e.message });
  }
};

module.exports = {
  index,
};
