const axios = require("axios");

const index = async (req, res) => {
  try {
    axios({
      "method": "GET",
      "url": "https://api.openweathermap.org/data/2.5/weather",
      "params": {
        "q": `${req.params.city},${req.params.country}`,
        "appid": "b3089a1608bf55629de9e36f6403da53"
      }
    })
    .then((response) => {
      console.log(response.data)
      res.send(response.data);
    })
    .catch((error) => {
      console.log(error)
      res.status(500).send({ status: 'ERROR', message: error });
    });

  } catch (e) {
    res.status(500).send({ status: 'ERROR', message: e.message });
  }
};

module.exports = {
  index,
};
