var validator = require("email-validator");

const validLogin = (req, res, next) => {
  try {
    
    const body = req.body;

    if(!body.hasOwnProperty('email') || !validator.validate(body.email))
    {
      throw {
        code: 400,
        status: 'ACCESS_DENIED',
        message: 'invalid email'
      };
    }

    if(!body.hasOwnProperty('password'))
    {
      throw {
        code: 400,
        status: 'ACCESS_DENIED',
        message: 'invalid password'
      };
    }

    if(body.hasOwnProperty('2fa') && !Number.isInteger(body["2fa"]))
    {
      throw {
        code: 400,
        status: 'ACCESS_DENIED',
        message: 'invalid 2fa'
      };
    }
    
    next();
  } catch (e) {
    res
      .status(e.code || 500)
      .send({ status: e.status || 'ERROR', message: e.message });
  }
};

module.exports = { validLogin };
