const express = require('express');
const averageController = require('../../controllers/v1/average-controller');

const router = express.Router();

router.get('/', averageController.index);

module.exports = router;
