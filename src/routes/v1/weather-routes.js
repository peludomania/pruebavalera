const express = require('express');
const weatherController = require('../../controllers/v1/weather-controller');

const router = express.Router();

router.get('/:country/:city', weatherController.index);

module.exports = router;
