const express = require('express');
const loginController = require('../../controllers/v1/login-controller');
const { validLogin } = require('../../middlewares/auth');


const router = express.Router();

router.post('/', validLogin,  loginController.index);

module.exports = router;
