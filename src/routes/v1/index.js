const averageRoutes = require('./average-routes');
const weatherRoutes = require('./weather-routes');
const loginRoutes = require('./login-routes');

module.exports = app => {
  app.use('/api/v1/average', averageRoutes);
  app.use('/api/v1/weather', weatherRoutes);
  app.use('/api/v1/login', loginRoutes);
};
